<footer>
    <a href="#header" class="footer-top">Вверх</a>
    <ul class="footer-menu footer-menu_navigation">
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/">На главную</a>
        </li>
    </ul>

    <ul class="footer-menu">
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userVk]'); ?>">ВКОНТАКТЕ</a>
        </li>
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userFacebook]'); ?>">FACEBOOK</a>
        </li>
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userInstagram]'); ?>">INSTAGRAM</a>
        </li>
    </ul>

    <div class="infobox">
        <div class="infobox__ultramarin">
            <a href="/"><img src="<?=get_template_directory_uri();?>/assets/images/logo_footer.png" alt="logo"></a>
            <p class="infobox__company-name">ULTRAMARIN RUSSIA</p>
        </div>
        <p class="story__text story__text_right">Дизайн и разработка</p>

        <div class="infobox__develop">
            <a target="_blank" href="https://facebook.com/blackandredmedia/">
                <img class="infobox__emblem" src="<?=get_template_directory_uri();?>/assets/images/br.png" alt="img">
            </a>
            <a target="_blank" href="https://facebook.com/aleksandr.tsapin/">
                <img class="infobox__emblem" src="<?=get_template_directory_uri();?>/assets/images/my-logo.png" alt="img">
            </a>
        </div>
    </div>

    <p class="copyright">Ultramarin Russia 2019. All rightes reserved.</p>
</footer>

<?php wp_footer(); ?>

<!-- Modal -->
<div class="modalbox">
    <div class="modalbox__content">
        <?php echo do_shortcode('[contact-form-7 id="5"]'); ?>
        <button class="modalbox__close">Закрыть</button>
    </div>
</div>

</body>
</html>