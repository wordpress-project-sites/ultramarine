$(document).ready(function() {

    $('#menu').on('click', function() {
        $('.topmenu__list').toggleClass('menu-active');
    });

    $('.topmenu__link').on('click', function() {
        $('.topmenu__list').removeClass('menu-active');
    });

    $('#modal').on('click', function() {
        $('.modalbox').toggleClass('modal-active');
        $('body').css('overflow', 'hidden');
    });

    $('.modalbox__close').on('click', function() {
        $('.modalbox').removeClass('modal-active');
        $('body').css('overflow', 'auto');
    });

    $(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".modalbox__content"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.modalbox').removeClass('modal-active');
            $('body').css('overflow', 'auto');
		}
    });

});